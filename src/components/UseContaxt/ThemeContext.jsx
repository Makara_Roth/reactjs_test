

import { createContext, useContext, useState } from "react";

const ThemeContext = createContext();
const UpdateThemeContext = createContext();

export function useTheme() {
    return useContext(ThemeContext);
}
export function useUpdateTheme() {
    return useContext(UpdateThemeContext);
}
export function ThemeProvider({ children }) {
    const [theme, setTheme] = useState(true);
    const changeTheme = () => {
        setTheme(!theme);
    }
    return (
        <ThemeContext.Provider value={theme}>
            <UpdateThemeContext.Provider value={changeTheme}>
                {children}
            </UpdateThemeContext.Provider>
        </ThemeContext.Provider>
    )}

