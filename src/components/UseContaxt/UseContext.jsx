


import { useUpdateTheme, useTheme } from "./ThemeContext";

const UseContext = () => {
  const theme = useTheme();
  const update = useUpdateTheme();

  const styled = {
    background: theme ? "white" : "black",
    height: "1000px",
    color: theme ? "black" : "white"
  }
  return (
    <div style={styled}>
      <h1>Hello World!!!</h1>
      <button onClick={update}>Them Change </button>
    </div>
  )
};
export default UseContext;