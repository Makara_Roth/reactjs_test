

import { useRef, } from "react";

const UseRef = () => {
  const count = useRef(0);
  const update = () => {
    count.current.style.color = "red"; 
     console.log(count.current);
  }
  return (
    <>
      <h1 ref={count} > The Time : {count.current}</h1>
      <button onClick={update}>Click me</button>
    </>
  )
}
export default UseRef;