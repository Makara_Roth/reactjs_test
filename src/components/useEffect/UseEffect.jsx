

import React, { useState, useEffect } from 'react';
const UseEffect = () => {
  const [name, setName] = useState("");
  const [count, setCount] = useState(-1)

  useEffect(() => {
    setCount(count + 1);
  }, [name]) // When name is change count will plus one more 

  const update = () => { // When click on the button the name is change 
    setName("Roth");
  }
  return (
    <div>
      <input value={name} onChange={(e) => setName(e.target.value)}/>
      <p>The Count {count}</p>
      <button onClick={update}>
        Click me
      </button>
    </div>
  );
}
export default UseEffect;
