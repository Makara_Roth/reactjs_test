

import { useReducer } from 'react';

const Reducer = (state, action) => {
    Switch (action.type) {
        case "increment":
            return { count: state.count + 1 };
            console.log(state.count)
        case "decrement":
            return { count: state.count - 1 };
        default:
            return state;
    };
}
const UseReducer = () => {
    const [state, dispatch] = useReducer(Reducer, { count: 0 })
    return (
        <>
            <h1 className='mt-5 p-4'> {state.count} </h1>
            <button className="m-2 btn text-bg-secondary p-3" onClick={(() => dispatch({ type: 'decrement' }))} > - </button>
            <button className='m-2 btn text-bg-secondary p-3' onClick={(() => dispatch({ type: 'increment' }))}> + </button>

        </>
    )
}
export default UseReducer;