
import React, { useEffect, useState } from 'react'

export default function UseCallBack({ counts }) {
    const [items, setItems] = useState([]);

    useEffect(() => {
        setItems(counts()); 
        console.log("updating")
    }, [counts])

    return items.map(item => <div key={item}> {item} </div>)

}