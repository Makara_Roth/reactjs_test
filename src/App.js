
import { useCallback, useState } from 'react';
import {BrowserRouter , Routes , Route}  from "react-router-dom";
import './App.css';
import UseCallBack from './components/UseCallBack/UseCallBack';
import "bootstrap/dist/css/bootstrap.css";
import UseMemo from './components/UseMemo/UseMemo';
import UseStatetest from "./components/Usestate/UseStatetest";
import UseEffect from "./components/useEffect/UseEffect";



const App = () => {
  return(
   <BrowserRouter>
       <Routes>
           <Route path="/" element={<UseStatetest/>}/>
           <Route path="UseEffcet" element={<UseEffect/>}/>
           <Route path="UasRef" element={<UseEffect/>}/>
           <Route path="UasRef" element={<UseEffect/>}/>
           <Route path="UasRef" element={<UseEffect/>}/>

       </Routes>
   </BrowserRouter>
  )


}
export default App;




















// function App() {
//   const [number, setNumber] = useState(0);
//   const [theme, setTheme] = useState(true);

//   const counts = useCallback(() => {
//     return [number, number + 1, number + 2];
//   }, [number])

//   const taggleTheme = {
//     backgroundColor: theme ? '#fff' : '#333',
//     color: theme ? '#333' : '#fff'
//   }

//   console.log(number)
//   return (
//     <div className='App mt-5' style={taggleTheme} >
//       <input
//         type='number'
//         value={number}
//         onChange={(e) => setNumber(parseInt(e.target.value))}
//       />
//       <button onClick={() => setTheme(theme => !theme)}> taggleTheme</button>
//       <UseCallBack counts={counts} />
//     </div>
//   )
// }
// export default App;
